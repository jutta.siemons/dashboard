# Readme: Dashboard 


## Installation

For an easy inatllation I suggest to work with a Conda environment.

1. Clone the following repository:  https://git.rwth-aachen.de/jutta.siemons/dashboard.git
2. If not already done, install Conda on your device. (https://conda.io/projects/conda/en/latest/user-guide/install/index.html)
3. Create a virtual environment with conda using the ```environment_dash.yml``` file.
```bash
$ conda env create --file environment_dash.yml
```
4. Activate the environment.
```bash
$ conda activate env_dash
```
5. Install dash[diskcache]
```bash
$ pip install "dash[diskcache]"
```

## Usage

1. In your file add
```
 from dashboard.dashboard import *
 ```
2. To add to your Dashboard,add 
```
@app.callback(
    Output('layout', 'children'),
    [Input('space', 'n_clicks')],
    [State('layout', 'children')],
)
def insert_name_here(n_clicks, children):
    children.append('something to add to your layout')
    return children
 ```
to create new elements.

3. Add Functions to your dashboard using the dash package.

3. Add the following to the end of your code.
```
if __name__ == '__main__':
    app.server.run(port=8000, host='127.0.0.1')
    app.run(debug=True)
```

## Example file
Two Example files are included in the package.

## Authors and acknowledgment
Author of this package is Jutta Siemons.
This package is basend on the paper and Git repository published by Yildiz et al. (https://www.frontiersin.org/articles/10.3389/feart.2022.1032438/full)