.. include:: ../../README.md
   :parser: myst_parser.sphinx_

Dashboard
===================

Using the dash package (https://dash.plotly.com), a 'stateless' dashboard is created with a set layout and multiple functions to either choose the coordinates by typing them in text areas or 
just click on the provided map.

The defaul should look like this:


.. image:: dashboard.png

.. automodule:: dashboard.dashboard
   :members:

Calculation
===================

To perform the calculations and create the histograms, the frontiers_yildizetal package and plotly package were used.

.. automodule:: dashboard.calculation
   :members:

Style
===================

For an easier access to the CSS style dictonary, Style has some preset styles (button, label, etc.) to let your dashboard look uniformly, but still lets you change some important aspects.
To get more freedom, the use of any CSS style dictonary is still possible.

.. autoclass:: dashboard.style.Style
   :members:

Figures
===================

To easily display error messages, empty plots or the map with the flow this class provides the used figures for this dashboard. 

.. autoclass:: dashboard.figures.Figures
   :members: