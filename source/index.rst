Welcome to dashboard's documentation!
=====================================

Because of Climate change, landslide risk assesment is getting more important every day.
But to calculate those run-out models, it is computationally very expensive.
One important part of this simulation is to verify its results by computing its uncertainties. 
That is why yildiz et al published the paper: Computationally-feasible uncertainty quantification in model-based landslide risk assessment(https://www.frontiersin.org/articles/10.3389/feart.2022.1032438/full) 
with a corresponding git repository. 
Whithin that package, one of three Monte Carlo Simulation data is being read and then Gaussian process emulators are created. 
With these, uncertainties cn be calculated.
To do so, relationships and histograms of impact area, deposit area, deposit volume, maximum flow velocity and maximum flow height for coulomb friction coefficient, turbulent friction coefficient and release volume were published.
This is were this package comes in handy: 
Values for maximum flow velocity and maximum flow height differ for each Coordinates, so an interactive dashboard is the solution.

In this python package, an interactive Dashboard was created to create relationships and histograms for maximum flow velocity and maximum flow height in an user friendly environment.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   files/dashcode





Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
