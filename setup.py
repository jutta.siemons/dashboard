from setuptools import setup

setup(
    name='dashboard',
    version='1.0',
    packages=['dashboard'],
    url='https://git.rwth-aachen.de/jutta.siemons/dashboard.git',
    author='Jutta Siemons',
    author_email='jutta.siemons@rwth-aachen.de',
    description='Creating a dashboard',
    python_requires='3.7',
    install_requires=[
        'numpy',
        'pandas',
        'dash',
        'plotly',
        'diskcache',
        'pillow',
        'ipykernel',
        'pyyaml',
        'rasterio',
        'requests',
        'rpy2',
        'scikit-learn',
        'r-base',
        'r-robustgasp'
    ],
)
