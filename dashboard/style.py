import copy


class Style:

    def __init__(self) -> None:
        """
        Constructor of the Style class
        """
        self.h1 = {
            'textAlign': 'center',
            'color': '#000000',
            'font-style': 'oblique',
            'background-color': '#FCE8FD'}
        self.p = {'textAlign': 'center', 'color': '#000000'}
        self.textarea = {'width': 100, 'height': 20, 'margin-top': 10}
        self.button = {
            'width': 100,
            'height': 40,
            'background-color': '#FCE8FD',
            'margin-left': 10,
            'margin-bottom': 10}
        self.label = {'whiteSpace': 'pre-line'}
        self.dropdown = {
            'width': 300,
            'height': 30,
            'background-color': '#FCE8FD'}
        self.empty = {}

    def getstyle(
            self,
            name,
            margin_top=None,
            margin_bottom=None,
            margin_left=None,
            margin_right=None,
            display=None,
            color=None,
            width=None,
            height=None,
            flex_direction=None):
        """
        creates a style dictonary

        Parameters
        ----------
        name : string
            name of a template, by default 'empty'
        margin_top : string, optional
            sets the margin top space, by default None
        margin_bottom : string, optional
            sets the bottom margin, by default None
        margin_left : string, optional
            sets the left margin, by default None
        margin_right : string, optional
            sets the right margin, by default None
        display : string, optional
            sets display setting, by default None
        color : string, optional
            sets color, by default None
        width : float, optional
            sets width, by default None
        height : float, optional
            sets height, by default None
        flex_direction : string, optional
            sets flex direction, by default None

        Returns
        -------
        dictonary
            dictonary with style settings

        Raises
        ------
        ValueError
            name input has to be valid
        TypeError
            margin input has to be a string of numbers
        TypeError
            margin input has to be a string of numbers
        TypeError
            margin input has to be a string of numbers
        TypeError
            margin input has to be a string of numbers
        ValueError
            display input has to be a valid string input
        ValueError
            felx direction has to be a valid string input
        """
        # error
        if name not in [
            'h1',
            'p',
            'label',
            'textarea',
            'button',
            'dropdown',
                'empty']:
            raise ValueError(
                'name has to one of h1,p,label,button,dropdown,empty')
        if margin_top is not None:
            if isinstance(margin_top, str):
                if not margin_top.isnumeric():
                    raise TypeError('margin top has to be a string of numbers')
        if margin_bottom is not None:
            if isinstance(margin_bottom, str):
                if not margin_bottom.isnumeric():
                    raise TypeError(
                        'margin bottom has to be a string of numbers')
        if margin_left is not None:
            if isinstance(margin_left, str):
                if not margin_left.isnumeric():
                    raise TypeError(
                        'margin left has to be a string of numbers')
        if margin_right is not None:
            if isinstance(margin_right, str):
                if not margin_right.isnumeric():
                    raise TypeError(
                        'margin right has to be a string of numbers')
        if display is not None:
            if isinstance(display, str):
                if display not in [
                    'inline',
                    'block',
                    'contents',
                    'flex',
                    'grid',
                    'inline-block',
                    'inline-flex',
                    'inline-grid',
                    'inline-table',
                    'list-item',
                    'run-in',
                    'table',
                    'table-caption',
                    'table-column-group',
                    'table-header-group',
                    'table-footer-group',
                    'table-row-group',
                    'table-cell',
                    'table-column',
                    'table-row',
                    'none',
                    'initial',
                        'inherit']:
                    raise ValueError('display has to be a valid style input')
        if flex_direction is not None:
            if isinstance(flex_direction, str):
                if flex_direction not in [
                    'row',
                    'row-reverse',
                    'column',
                    'column-reverse',
                    'initial',
                        'inherit']:
                    raise ValueError(
                        'flex direction has to be a valid style input')
        style = copy.copy(getattr(self, name))
        if margin_top is not None:
            style['margin-top'] = int(margin_top)
        if margin_bottom is not None:
            style['margin-bottom'] = int(margin_bottom)
        if margin_left is not None:
            style['margin-left'] = int(margin_left)
        if margin_right is not None:
            style['margin-right'] = int(margin_right)
        if display is not None:
            style['display'] = display
        if flex_direction is not None:
            style['flex-direction'] = flex_direction
        return style
