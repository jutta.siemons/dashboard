
import numpy as np
from dashboard.frontiers_yildizetal.emulators import ScalarEmulators
from dashboard.frontiers_yildizetal.utilities import data
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import pandas as pd


def calculate_histograms(x, y, mc_num):
    """
    calculates the histograms

    Parameters
    ----------
    x : int
        x coordinate
    y : int
        y coordinate
    mc_num : int
        number of monta carlo simulation

    Returns
    -------
    figure
       figure with the histograms

    Raises
    ------
    TypeError
        x input has to be an integer
    TypeError
        y input has to be an integer
    TypeError
        MC Simulation number has to be 1, 2 or 3
    """
    if not isinstance(x, int):
        raise TypeError('x has to be a int')
    if not isinstance(y, int):
        raise TypeError('y has to be a int')
    if not isinstance(mc_num, int):
        if mc_num not in [1, 2, 3]:
            raise TypeError('mc number has to be 1, 2 or 3')
    if mc_num == 1:
        mc = str('mcs1')
    if mc_num == 2:
        mc = str('mcs2')
    if mc_num == 3:
        mc = str('mcs3')
    acheron = ScalarEmulators('acheron', threshold=0.1, loc_x=x, loc_y=y)
    input_mcs3 = data.load_input('acheron', mc)
    data_calc = pd.DataFrame(input_mcs3, columns=['Zero', 'One', 'Two'])
    scalars = list(acheron.output.keys())
    for scalar in scalars:
        data_calc[scalar] = np.transpose(
            acheron.predict_scalar(
                scalar, input_mcs3)[0])

    fig = make_subplots(rows=2, cols=4, shared_xaxes=True, shared_yaxes=True)
    fig.append_trace(
        go.Histogram2d(
            x=data_calc['Zero'],
            y=data_calc['vmax'],
            colorscale='Viridis',
            showscale=False,
            autobinx=False,
            xbins=dict(
                start=0,
                end=0.3,
                size=0.3 / 30),
            autobiny=False,
            ybins=dict(
                start=15,
                end=45,
                size=(
                    45 - 15) / 30)),
        row=1,
        col=1)
    fig.append_trace(
        go.Histogram2d(
            x=data_calc['One'],
            y=data_calc['vmax'],
            colorscale='Viridis',
            showscale=False,
            autobinx=False,
            xbins=dict(
                start=0,
                end=2000,
                size=2000 / 30),
            autobiny=False,
            ybins=dict(
                start=15,
                end=45,
                size=(
                    45 - 15) / 30)),
        row=1,
        col=2)
    fig.append_trace(
        go.Histogram2d(
            x=data_calc['Two'],
            y=data_calc['vmax'],
            colorscale='Viridis',
            showscale=False,
            autobinx=False,
            xbins=dict(
                start=2.5,
                end=10,
                size=(
                    10 - 2.5) / 30),
            autobiny=False,
            ybins=dict(
                start=15,
                end=45,
                size=(
                    45 - 15) / 30)),
        row=1,
        col=3)
    fig.append_trace(
        go.Histogram(
            y=data_calc['vmax'],
            showlegend=False,
            autobiny=False,
            ybins=dict(
                start=15,
                end=45,
                size=(
                    45 - 15) / 30),
            autobinx=False,
            xbins=dict(
                start=0,
                end=1200,
                size=1200 / 30)),
        row=1,
        col=4)
    fig.append_trace(
        go.Histogram2d(
            x=data_calc['Zero'],
            y=data_calc['hmax'],
            colorscale='Viridis',
            showscale=False,
            autobinx=False,
            xbins=dict(
                start=0,
                end=0.3,
                size=0.3 / 30),
            autobiny=False,
            ybins=dict(
                start=15,
                end=60,
                size=(
                    60 - 15) / 30)),
        row=2,
        col=1)
    fig.append_trace(
        go.Histogram2d(
            x=data_calc['One'],
            y=data_calc['hmax'],
            colorscale='Viridis',
            showscale=False,
            autobinx=False,
            xbins=dict(
                start=0,
                end=2000,
                size=2000 / 30),
            autobiny=False,
            ybins=dict(
                start=15,
                end=60,
                size=(
                    60 - 15) / 30)),
        row=2,
        col=2)
    fig.append_trace(
        go.Histogram2d(
            x=data_calc['Two'],
            y=data_calc['hmax'],
            colorscale='Viridis',
            showscale=False,
            autobinx=False,
            xbins=dict(
                start=2.5,
                end=10,
                size=(
                    10 - 2.5) / 30),
            autobiny=False,
            ybins=dict(
                start=15,
                end=60,
                size=(
                    60 - 15) / 30)),
        row=2,
        col=3)
    fig.append_trace(
        go.Histogram(
            y=data_calc['hmax'],
            showlegend=False,
            autobiny=False,
            ybins=dict(
                start=15,
                end=60,
                size=(
                    60 - 15) / 30),
            autobinx=False,
            xbins=dict(
                start=0,
                end=1200,
                size=1200 / 30)),
        row=2,
        col=4)
    fig.update_yaxes(title_text="Max. flow velocity", row=1, col=1)
    fig.update_yaxes(title_text="Max. flow height", row=2, col=1)
    fig.update_xaxes(title_text="Coulomb fric. coef.", row=2, col=1)
    fig.update_xaxes(title_text="Turbulent fric. coef.", row=2, col=2)
    fig.update_xaxes(title_text="Release Volume", row=2, col=3)
    fig.update_xaxes(title_text="Counts", row=2, col=4)
    fig.update_layout(
        autosize=False,
        width=700,
        height=600,
        margin=dict(
            l=20,
            r=20,
            t=30,
            b=20),
        title_text="Result for x: " +
        str(x) +
        " y: " +
        str(y))
    return fig
