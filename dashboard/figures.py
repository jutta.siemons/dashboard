from dash import *
import pandas as pd
import numpy as np
import plotly.graph_objects as go
import plotly.express as px
from PIL import Image
from plotly.subplots import make_subplots
from dashboard.frontiers_yildizetal.ravaflow import Simulations
from dashboard.frontiers_yildizetal.emulators import VectorEmulators


class Figures:
    def __init__(self, map, box, case) -> None:
        """
        constructor of the class Figures

        Parameters
        ----------
        map : string
            path to map picture
        box : int
            bounding box of the coordinates
        case : str
            synth or acheron case

        Returns
        -------
        figure
            figure containing the flow map

        Raises
        ------
        TypeError
            map input is no string
        TypeError
            box input is no list
        TypeError
            box input is no 2x2 matrix
        TypeError
            case has to be acheron or synth
        """
        if not isinstance(map, str):
            raise TypeError('map input has to be a string')
        if not isinstance(box, list):
            raise TypeError('box input has to be float')
        if len(box) is not 2 or len(box[0]) is not 2:
            raise TypeError('box input has to be a 2x2 matrix')
        if not isinstance(case, str):
            if case not in ['acheron', 'synth']:
                raise TypeError('case has to be acheron or synth')
        self.fig_empty = self.create_empty_figure()
        self.box = box
        self.fig_map = self.create_map_figure(map, box, case)

    def create_map_figure(self, map, box, case):
        """
        creates the figure of the flow

        Parameters
        ----------
        map : string
            path to map picture
        box : int
            bounding box of the coordinates
        case : str
            synth or acheron case

        Returns
        -------
        figure
            figure containing the flow map

        Raises
        ------
        TypeError
            map input is no string
        TypeError
            box input is no list
        TypeError
            box input is no 2x2 matrix
        TypeError
            case has to be acheron or synth
        """
        if not isinstance(map, str):
            raise TypeError('map input has to be a string')
        if not isinstance(box, list):
            raise TypeError('box input has to be float')
        if len(box) is not 2 or len(box[0]) is not 2:
            raise TypeError('box input has to be a 2x2 matrix')
        if not isinstance(case, str):
            if case not in ['acheron', 'synth']:
                raise TypeError('case has to be acheron or synth')
        ac = VectorEmulators(case, qoi='hmax', threshold=0.1)
        ac_pem = Simulations(case + '_pem').create_vector(
            qoi='hmax', threshold=0.1, valid_cols=ac.valid_cols
        )
        pem3_mean = np.zeros((1, ac.rows * ac.cols))
        pem3_mean[:, ac.valid_cols.nonzero()] = ac_pem[0][16:24].mean(axis=0)
        pem3_mean = pem3_mean.reshape(ac.rows, ac.cols)
        pem3_mean_ma = np.where(pem3_mean > 0.1, pem3_mean, None)
        x = np.linspace(box[0][0], box[0][1], len(pem3_mean_ma[0]))
        y = np.linspace(box[1][1], box[1][0], len(pem3_mean_ma))
        fig_map = go.Figure()
        fig_map.add_trace(
            go.Heatmap(
                z=pem3_mean_ma,
                x=x,
                y=y,
                colorscale='viridis',
                connectgaps=False,
                showscale=False,
                hoverinfo='x+y+z'))
        fig_map.add_layout_image(
            dict(
                source=Image.open(map),
                xref="x",
                yref="y",
                x=box[0][0],
                y=box[1][1],
                sizex=box[0][1] -
                box[0][0],
                sizey=box[1][1] -
                box[1][0],
                opacity=1,
                sizing='stretch',
                layer="below"))
        x_ax = np.arange(box[0][0], box[0][1], 1000)
        y_ax = np.arange(box[1][0], box[1][1], 1000)
        fig_map.update_layout(
            autosize=False, width=450, height=600, margin=dict(
                l=20, r=20, t=0, b=20), xaxis=dict(
                    tickmode='array', tickvals=x_ax, ticktext=[
                        '1.489', '1.490', '1.491', '1.492', '1.493'], showgrid=False), yaxis=dict(
                            tickmode='array', tickvals=y_ax, ticktext=[
                                '5.201', '5.202', '5.203', '5.204', '5.205'], showgrid=False))
        return fig_map

    def create_empty_figure(self):
        """
        creates an empty calculation figure

        Returns
        -------
        figure
            empty figure
        """
        fig_empty = make_subplots(
            rows=2,
            cols=4,
            shared_xaxes=True,
            shared_yaxes=True)
        fig_empty.append_trace(go.Scatter(x=[], y=[]), row=1, col=1)
        fig_empty.append_trace(go.Scatter(x=[], y=[]), row=1, col=2)
        fig_empty.append_trace(go.Scatter(x=[], y=[]), row=1, col=3)
        fig_empty.append_trace(go.Scatter(x=[], y=[]), row=1, col=4)
        fig_empty.append_trace(go.Scatter(x=[], y=[]), row=2, col=1)
        fig_empty.append_trace(go.Scatter(x=[], y=[]), row=2, col=2)
        fig_empty.append_trace(go.Scatter(x=[], y=[]), row=2, col=3)
        fig_empty.append_trace(go.Scatter(x=[], y=[]), row=2, col=4)
        fig_empty.update_yaxes(
            title_text="Max. flow velocity",
            row=1,
            col=1,
            showgrid=False)
        fig_empty.update_yaxes(
            title_text="Max. flow height",
            row=2,
            col=1,
            showgrid=False)
        fig_empty.update_yaxes(showgrid=False)
        fig_empty.update_xaxes(
            title_text="Coulomb fric. coef.",
            row=2,
            col=1,
            showgrid=False)
        fig_empty.update_xaxes(
            title_text="Turbulent fric. coef.",
            row=2,
            col=2,
            showgrid=False)
        fig_empty.update_xaxes(
            title_text="Release Volume",
            row=2,
            col=3,
            showgrid=False)
        fig_empty.update_xaxes(
            title_text="Counts",
            row=2,
            col=4,
            showgrid=False)
        fig_empty.update_xaxes(showgrid=False)
        fig_empty.update_layout(
            autosize=False,
            width=700,
            height=600,
            margin=dict(
                l=20,
                r=20,
                t=30,
                b=20))
        return fig_empty

    def fig_error(self, message):
        """
        creates the a figure with the error message

        Parameters
        ----------
        message : str
            error messsage

        Returns
        -------
        figure
            figure with error message

        Raises
        ------
        TypeError
            message input is no string
        """
        if not isinstance(message, str):
            raise TypeError('message input has to be a string')
        fig_error = px.scatter_3d().add_annotation(
            text=message,
            showarrow=False,
            font={
                "size": 20})
        return fig_error
