from dash import *
import numpy as np
import dashboard.calculation as calc
from dashboard.figures import Figures
from dash.long_callback import DiskcacheLongCallbackManager
import diskcache
from dashboard.style import Style

cache = diskcache.Cache("./cache")
long_callback_manager = DiskcacheLongCallbackManager(cache)
background_callback_manager = DiskcacheManager(cache)
app = Dash(__name__, background_callback_manager=background_callback_manager)
figure = Figures(map='dashboard/input/fig8.png',
                 box=[[1489000, 1493600], [5201000, 5205001]], case='acheron')
styles = Style()
app.layout = html.Div(
    id='layout',
    style=styles.getstyle(
        'empty',
        flex_direction='column'),
    children=[
        html.H1(
            children='Dashboard for relationships and histograms of maximum flow velocity and maximum flow ',
            style=styles.getstyle('h1')),
        html.P(
            children='''In addition to: Computationally-feasible uncertainty quantification in model-based landslide risk assessment''',
            style=styles.getstyle('p')),
        html.Div(
            style=styles.getstyle(
                'empty',
                display='flex',
                flex_direction='row'),
            children=[
                html.Div(
                    children=[
                        dcc.Textarea(
                            id='x-coordinate',
                            value='',
                            placeholder='Enter x-Coordinate',
                            style=styles.getstyle('textarea')),
                        dcc.Textarea(
                            id='y-coordinate',
                            value='',
                            placeholder='Enter y-Coordinate',
                            style=styles.getstyle('textarea')),
                        html.Button(
                            'Submit',
                            id='text-input-button',
                            n_clicks=0,
                            style=styles.getstyle('button')),
                        html.Div(
                            children=[
                                html.Label(
                                    'You have not clicked coordinates',
                                    id='click-data',
                                    style=styles.getstyle(
                                        'label',
                                        display='inline')),
                                html.Button(
                                    'Submit',
                                    id='click-input-button',
                                    n_clicks=0,
                                    style=styles.getstyle('button')),
                                dcc.Graph(
                                    id='mean-flow-height',
                                    figure=figure.fig_map),
                            ]),
                    ]),
                html.Div(
                    children=[
                        html.Label(
                            'Please choose your Monte carlo Simulation:',
                            id='mc-text',
                            style=styles.getstyle(
                                'label',
                                display='block')),
                        dcc.Dropdown(
                            [
                                'Monte Carlo Simulation 1',
                                'Monte Carlo Simulation 2',
                                'Monte Carlo Simulation 3'],
                            'Monte Carlo Simulation 3',
                            id='dropdown-mc',
                            style=styles.getstyle(
                                'dropdown',
                                display='inline-block')),
                        dcc.Graph(
                            id='output-graph',
                            figure=figure.fig_empty)]),
            ]),
        dcc.Store(
            id='mc-value',
            storage_type='session'),
        html.Button(
            id='space',
            n_clicks=0,
            style=styles.getstyle(
                'empty',
                display='none')),
    ])


@callback(Output('mc-value', 'data'),
          Input('dropdown-mc', 'value'))
def assign_data(value):
    """
    set global variable

    Parameters
    ----------
    value : int
        number of monte carlo simulation

    Returns
    -------
    int
        number of monte carlo simulation
    """
    mc_simulation = int(value[-1])
    return mc_simulation


@app.long_callback(
    output=Output('output-graph', 'figure'),
    inputs=dict(text_submit=Input('text-input-button', 'n_clicks'),
                click_submit=Input('click-input-button', 'n_clicks'),
                click_coor=Input('mean-flow-height', 'clickData'),
                ),
    state=dict(x_coord=State('x-coordinate', 'value'),
               y_coord=State('y-coordinate', 'value'),
               mc_simulation=State('mc-value', 'data')),
    running=[(Output("text-input-button", "disabled"), True, False),
             (Output("click-input-button", "disabled"), True, False)],
    prevent_initial_call=True)
def submit_calculations(
        text_submit,
        click_submit,
        click_coor,
        x_coord,
        y_coord,
        mc_simulation):
    """
    calculates the histograms

    Parameters
    ----------
    text_submit : int
        number of times button clickes
    click_submit : int
        number of time buttons clicked
    click_coor : dictonary
        dictonary with data that was clicked
    x_coord : int
        x coordinate
    y_coord : int
        y coordinate
    mc_simulation : int
        number of monte carlo simulation

    Returns
    -------
    figure
        returns a figure corresponding to the input

    Raises
    ------
    Error
        x input has to be an integer between 1488990 and 1493491
    Error
        y input has to be an integer between 5200290 and 5205750
    Error
        coordinates need to be where flow is
    """
    triggered_id = ctx.triggered_id
    if triggered_id == 'text-input-button':

        if int(x_coord) not in range(
                figure.box[0][0],
                figure.box[0][1]) or not x_coord.isnumeric():
            return figure.fig_error('Please enter a integer x-coordinate between ' + str(
                figure.box[0][0]) + 'and' + str(figure.box[0][1]))
        if int(y_coord) not in range(
                figure.box[1][0],
                figure.box[1][1]) or not y_coord.isnumeric():
            return figure.fig_error('Please enter a integer y-coordinate between ' + str(
                figure.box[1][1]) + 'and' + str(figure.box[1][1]))
        fig_data_z = figure.fig_map.to_dict()['data'][0]['z']
        x = np.linspace(figure.box[0][0], figure.box[0][1], len(fig_data_z[0]))
        y = np.linspace(figure.box[1][1], figure.box[1][0], len(fig_data_z))
        x_pos = (np.abs(x - int(x_coord))).argmin()
        y_pos = (np.abs(y - int(y_coord))).argmin()
        if figure.fig_map.to_dict()['data'][0]['z'][y_pos][x_pos] is None:
            return figure.fig_error(
                'Please choose coordinates where there is flow.')
        figure_text = calc.calculate_histograms(
            x=int(x_coord), y=int(y_coord), mc_num=mc_simulation)
        return figure_text
    if triggered_id == 'click-input-button':
        if click_coor['points'][0]['z'] is None:
            figure_err = figure.fig_error(
                'Please choose coordinates where there is flow.')
            return figure_err
        figure_click = calc.calculate_histograms(
            x=int(
                click_coor['points'][0]['x']), y=int(
                click_coor['points'][0]['y']), mc_num=mc_simulation)
        return figure_click
    if triggered_id == 'mean-flow-height':
        return figure.fig_empty


@callback(
    Output('click-data', 'children'),
    Input('mean-flow-height', 'clickData'),
    prevent_initial_call=True)
def display_click_data(clickData):
    """
    displays the coordinates that were clickes

    Parameters
    ----------
    clickData : dictonary
        dictonary with data that was clicked

    Returns
    -------
    string
        message to be displayed
    """
    if clickData is not None:
        return ('You have clicked: ' +
                str(int(clickData['points'][0]['x'])) +
                ' ' +
                str(int(clickData['points'][0]['y'])))
    return ('You have not clicked coordinates')
