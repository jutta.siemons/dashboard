from dashboard.dashboard import *


@app.callback(
    Output('layout', 'children'),
    [Input('space', 'n_clicks')],
    [State('layout', 'children')],
)
def add_to_layout(n_clicks, children):
    children.append(html.H1('Something', id='something'))
    return children


if __name__ == '__main__':
    app.server.run(port=8000, host='127.0.0.1')
    app.run(debug=True)
